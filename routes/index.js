/**
 * Created by andreassprotte on 28/10/16.
 */
let express = require('express'),
router = express.Router(),
  helpers = require('../helpers/helpers.js'),
  path = require("path"),
  mongoose = require('mongoose'),
  ObjectId = require('mongodb').ObjectID;

let libRoot = path.join(__dirname, '../');

/* GET weather listing by id */
router.get('/', helpers.isAdmin, function(req, res, next) {
  //get list of models
  let models = [];
  for(let prop in mongoose.models){
    if(mongoose.models[prop].schema.options.editable){
      models.push({name: prop, model: mongoose.models[prop]})
    }
  }
  res.render(libRoot + 'views/index', { list: models, title: 'list' });
});

//paths to models:
let modelNames = Object.keys(mongoose.models);
let models = {};

for (let i=0; i < modelNames.length; i++){

  //require defined models
  models[modelNames[i]] = mongoose.model(modelNames[i]);

  router.get('/'+modelNames[i], helpers.isAdmin, function(req, res, next) {
    let modelName = req.url.split('/').join("");
    let model = models[modelName];
    let modelProps = mongoose.model(modelName).schema.paths;
    let editableProps = editableModelProps(modelName);
    let rels = getPopulates(modelProps);
    editableProps = removeDubKeys(editableProps, rels.rels);
    model.find({})
      .populate(rels.populates)
      .exec(function(err, items){
        res.render(libRoot +  'views/list', { title: modelName, items: items, modelName: modelName, modelProps: modelProps, editableProps: editableProps, message: req.body.messsage, rels: rels.rels });
    });
  });

  //new form method. GET modelName/new
  router.get('/'+modelNames[i]+'/new', helpers.isAdmin, function(req, res, next) {
    let modelName = req.url.split('/')[1];
    let modelProps = mongoose.model(modelName).schema.paths;
    let editableProps = editableModelProps(modelName);
    res.render(libRoot +  'views/new', { title: 'new' + modelName, modelProps: modelProps, modelName: modelName, editableProps: editableProps });
  });

  //new method. post modelName/new
  router.post('/'+modelNames[i], helpers.isAdmin, function(req, res, next) {
    let modelName = req.url.split('/')[1];
    let model = models[modelName];
    let modelProps = mongoose.model(modelName).schema.paths;

    let obj = {};
    let editableProps = editableModelProps(modelName);
    for(let i=0; i<editableProps.length; i++){
      if(req.body[editableProps[i]]){
        obj[editableProps[i]] = req.body[editableProps[i]]
      }
    }
    model.create(obj, (err, item) => {
      if(err || !item){
        if(err) { req.flash('error',{type: 'danger', text: JSON.stringify(err) }) }
        else if(!item) {
          req.flash('error',{type: 'danger', text: 'something went wrong' });
        }
        res.render(libRoot +  'views/new', { title: 'new' + modelName, modelProps: modelProps, modelName: modelName, editableProps: editableProps });
      } else{
        req.flash('success',{type: 'success', text: 'successfully created' });
        res.redirect('/admin/'+modelName);
      }
    });
  });

  //SHOW method. GET modelName/:id
  router.get('/'+modelNames[i]+'/:id', helpers.isAdmin, function(req, res, next) {
    let modelName = req.url.split('/')[1];
    let model = models[modelName];
    let modelProps = mongoose.model(modelName).schema.paths;
    let editableProps = editableModelProps(modelName);
    model.findOne(ObjectId(req.params.id), function(err, item){
      res.render(libRoot +  'views/edit', { title: modelName, item: item, modelName: modelName, modelProps: modelProps, editableProps: editableProps });
    });
  });

  // Post method for editing item
  router.post('/'+modelNames[i]+'/:id/edit', helpers.isAdmin, function(req, res, next) {
    let modelName = req.url.split('/')[1];
    let model = models[modelName];
    let modelProps = mongoose.model(modelName).schema.paths;

    let update = {};
    let editableProps = editableModelProps(modelName);
    for(let i=0; i<editableProps.length; i++){
      if(req.body[editableProps[i]]){
        update[editableProps[i]] = req.body[editableProps[i]]
      }
    }
    model.findOneAndUpdate({_id:req.params.id}, update, {new: true}, (err, item) => {
      if(err) { req.flash('error', {type: 'danger', text: JSON.stringify(err) } ) }
      else if(!item) { req.flash('error', {type: 'danger', text: 'not found' } ) }
      else{ req.flash('info', {type: 'success', text: 'successfully updated' } )}
      res.redirect('/admin/'+modelName);
    });
  });

  //DELETE method. GET modelName/:id/delete
  router.get('/'+modelNames[i]+'/:id/delete', helpers.isAdmin, function(req, res, next) {
    let modelName = req.url.split('/')[1];
    let model = models[modelName];
    model.findOne({_id: req.params.id}, function(err, item){
      if(err || !item){
        if(err) { req.flash('error', { type: 'danger', text: JSON.stringify(err) } ); }
        else if(item.result.n === 0) { req.flash('error', {type: 'danger', text: 'not found' } ); }
        return res.redirect('/admin/' + modelName);
      }else{
        item.remove((err) => {
          if(err) { req.flash('error', {type: 'danger', text: JSON.stringify(err) } ) }
          else{ req.flash('info', {type: 'success', text: 'item was successfully deleted' } ); }
          return res.redirect('/admin/' + modelName);
        });
      }
    });
  });

}

//return array of editable model properties
function editableModelProps(modelName){
  let editableProps = [];
  for(prop in mongoose.model(modelName).schema.paths){
    if(mongoose.model(modelName).schema.paths[prop].options.editable){
      editableProps.push(prop);
    }
  }
  return editableProps;
}

// getPopulates() finds all associations on model and creates populates object to use in mongoose query
function getPopulates(mp){
  let populates = [];
  let rels = [];
  if(mp){
    for(let p in mp){
      if(mp[p].instance && mp[p].instance === 'ObjectID' && mp[p].path !== "_id"){
        //then is rel
        rels.push(mp[p].path);
        populates.push({path: mp[p].path})
      }
    }
  }
  return {populates: populates, rels: rels};
}

function removeDubKeys(a,b){
  for(let i=0; i<a.length;i++){
    for(let j=0; j<b.length;j++) {
      if (a[i] === b[j]) {
        a.splice(i,1);
      }
    }
  }
  return a;
}

module.exports = router;