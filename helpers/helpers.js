/**
 * Created by andreassprotte on 28/10/16.
 */
var helpers = {
  isLoggedIn : function(req, res, next) {
    if (req.isAuthenticated()){
      return next ? next() : true;
    }else{
      return res ? res.redirect('/login') : null;
    }
  },
  isAdmin : function(req, res, next){
    //TODO: remove
    //return next ? next() : true;

    //TODO: reinstate
    if (req && req.isAuthenticated()  && req.user.role && req.user.role == 'admin'){
      return next ? next() : true;
    }else{
      return res ? res.redirect('/login') : null;
    }
  }
};

module.exports = helpers;